from .sqlite_web import app, initialize_app

__all__ = ['app','initialize_app']
